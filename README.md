# ZDP Library v1.2

**[Official ZDP Discord](https://discord.gg/YADGVW8zWv)**

**Installation Instructions:**

Download and unzip "zdp-library" folder into your custom scenery folder. If you have an older version installed, delete it before installing the newest version. 

It is recommended to use x-organizer to ensure correct scenery load order. This may also be done manually by opening the scenery_packs.ini file in the "Custom Scenery" folder, and moving zdp-library below your airports but above orthos. See [this forum post](https://forums.x-plane.org/index.php?/forums/topic/256489-correct-order-of-the-scenery_packsini/) for more information on scenery load orders. 

Please report any bugs on the official ZDP discord (linked above)

To conctact me about this repo please send me a message on Discord @StableSystem

This repository and it's contents are protected under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)

**Usage Guide**

*Markings*

	Lines - Use these just like any other library. 
	
	Paint - Draw paint over top of existing ground textures. These will automatically be rendered on top of any concrete or asphalt textures. Paint is great for filling in large areas or making custom runways. Use the "paint_border" lines to draw a border around your painted shape. 
	
	Signs - These are done as subtextured sheets. Click on the letter you want to place and then draw it as a rectangle to create a polygon. You can click a letter multiple times to change the edges. These use the same coordinates as the default xplane signs so upgrading existing signs is as easy as changing the resource value. 
	
	Painted Text - Good for things like gate numbers. Place a circle (or draw your own shape using paint) and place your text on top. 
	
	Runway Markings - Use these objects to draw markings on runways. Use the associated "_border" objects to add a black border if needed. 
	
	Grunge - place these as objects. Don't be afraid to overlap a few on top of eachother if you want a darker effect! 

*Ground Textures*

	[Video Tutorial](https://youtu.be/ZHQ5r5koGZg)
	
	Step 1: Place the base textures (concrete or asphalt). Make note of the naming convention for base textures to ensure you get exactly what you need. Note that WED doesn't show decals in the preview, you'll need to load into xplane to see the full effect. All variation and surface types and be seen from the included Surface_Guide.png and Variation_Guide.png images. 
	
	>	<Surface>_<Wear Level>_<Variation Type>.pol

	>	ex. concrete/flat_worn_striped.pol is a flat concrete slab with a worn surface, and randomly varying darkness along one axis
	
	Step 2: Place your overlays. Duplicate each surface, pick the color or darkness you want your surface to have and replace the "resource" value to apply the overlay. Overlays can be stacked too if you need even more control! Just be sure to use the overlay specified for the surface you are applying it to as each overlay is tuned specifically for the surface it is used on top of. 

	Step 3 (Optional): Create a custom grunge mask. See the video linked above for an overview of what this is. This is a more advanced technique but provides signifigant visual improvements. 
	
	Step 4 (Optional): Add spot grunge. Using the spot grunge objects, place around the airport to improve the realism and break up large uniform surfaces. 

**Credits**

ZDP Developers: StableSystem, TJ, Awfsiu

External Contributors: joshomoore

